<?php
/**
 * The following snippets uses `PLUGIN` to prefix
 * the constants and class names. You should replace
 * it with something that matches your plugin name.
 */
// define test environment
define('AWIS_USER_TABLE_PHPUNIT', true);

// define fake ABSPATH
if (!defined('ABSPATH')) {
	define('ABSPATH', sys_get_temp_dir());
}
// define fake PLUGIN_ABSPATH
if (!defined('AWIS_USER_TABLE_ABSPATH')) {
	define('AWIS_USER_TABLE_ABSPATH', sys_get_temp_dir() . '/app/plugins/user-table/');
}

require_once __DIR__ . '/../../vendor/autoload.php';

// Include the class for PluginTestCase
require_once __DIR__ . '/Inc/PluginTestCase.php';

// Since our plugin files are loaded with composer, we should be good to go