<?php 

use Brain\Monkey\Functions;
use \App\Core\Activate;

class ActivateTest extends \PluginTestCase {

    function test_activatePlugin()
    {

        Functions\expect('add_option')
        ->once()
        ->with('custom_endpoint', 'awis-custom-endpoint')
        ->andReturn(true);

        Functions\expect('flush_rewrite_rules')
        ->once()
        ->with(true)
        ->andReturn(null);

        
        Activate::activatePlugin();
    }

}