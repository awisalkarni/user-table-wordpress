<?php 

use Brain\Monkey\Functions;
use \App\Api\SettingsApi;

class SettingsApiTest extends \PluginTestCase {

    public function test_Register()
    {
        $class = new SettingsApi;
        
        Functions\expect('add_action')
        ->once()
        ->with('admin_menu', [$class, 'addAdminMenu'])
        ->andReturn(true);

        $pages = [
            [
                'page_title' => 'User Table',
                'menu_title' => 'User Table',
                'capability' => 'manage_options',
                'menu_slug' => 'user_table_plugin',
                'callback' => function(){},
                'icon_url' => 'dashicons-admin-site-alt3',
                'position' => 100,
            ],
        ];

        //need to set pages var first
        $class->addPages($pages);
        $class->register();
    }
    
}