<?php 

use Brain\Monkey\Functions;
use \App\Pages\Admin;

class AdminTest extends \PluginTestCase {

    protected $class;

    public function setUp(): void
    {
        parent::setUp();
        $this->class = new Admin;
        $this->class->register();
    }

    public function test_initPages()
    {
        $reflectionClass = new \ReflectionClass('\App\Pages\Admin');
        $pages = $reflectionClass->getProperty('pages');
        $pages->setAccessible(true);

        $this->assertNotEmpty($pages);
    }

}