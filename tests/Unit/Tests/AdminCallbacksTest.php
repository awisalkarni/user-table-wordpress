<?php 

use Brain\Monkey\Functions;
use \App\Api\Callbacks\AdminCallbacks;

class AdminCallbacksTest extends \PluginTestCase
{

    protected $class;

    public function setUp() : void 
    {
        parent::setUp();
        $this->class = new AdminCallbacks;
    }

    public function test_adminDashboard()
    {
        Functions\expect('settings_fields')
        ->once()
        ->with('usertable_options_group')
        ->andReturn(true);

        Functions\expect('settings_errors')
        ->once()
        ->andReturn(null);

        Functions\expect('do_settings_sections')
        ->once()
        ->with('user_table_plugin')
        ->andReturn(null);

        Functions\expect('get_option')
        ->twice()
        ->with("custom_endpoint")
        ->andReturn(\Mockery::type('string'));

        Functions\expect('get_home_url')
        ->once()
        ->andReturn(\Mockery::type('string'));

        Functions\expect('esc_url')
        ->twice()
        ->andReturn(\Mockery::type('string'));

        Functions\expect('submit_button')
        ->once()
        ->andReturn('<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">');

        $this->setOutputCallback(function() {});

        
        $this->class->adminDashboard();
    }

    public function test_usertableSectionsGroup()
    {
        $expected = 'Enter your custom. 
        With “custom endpoint” we mean an arbitrary URL not recognized by WP as a standard URL, 
        like a permalink or so.';

        $this->expectOutputString($expected);
        $this->class->usertableSectionsGroup([]);
    }

    public function test_usertableUserUrlField()
    {

        Functions\expect('esc_attr')
        ->once()
        ->andReturn(\Mockery::type('string'));

        Functions\expect('get_option')
        ->once()
        ->with("custom_endpoint")
        ->andReturn(\Mockery::type('string'));

        Functions\expect('wp_kses')
        ->once()
        ->with(\Mockery::type('string'), [
            'input' => [
                'type' => [],
                'name' => [],
                'value' => [],
                'placeholder' => [],
            ],
        ])
        ->andReturn(\Mockery::type('string'));

        $this->setOutputCallback(function() {}); //suppress output

        $this->class->usertableUserUrlField([]);
    }

}