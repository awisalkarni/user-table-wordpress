<?php 

use Brain\Monkey\Functions;
use \App\Init;

class InitTest extends \PluginTestCase {

    public function test_services()
    {
        $classes = [
            App\Pages\Admin::class,
            App\Core\Enqueue::class,
            App\Core\SettingsLinks::class,
            App\Pages\FrontEnd::class,
        ];

        $response = Init::services();

        $this->assertSame($response, $classes);
    }

    public function test_instantiate()
    {
        $reflection = new \ReflectionClass('\App\Init');

        $method = $reflection->getMethod('instantiate');
        $method->setAccessible(true);

        $service = 'App\Pages\Admin';
        $response = $method->invokeArgs(new Init(), [$service]);

        $admin = new \App\Pages\Admin;

        $this->assertEquals($admin, $response);
    }
}