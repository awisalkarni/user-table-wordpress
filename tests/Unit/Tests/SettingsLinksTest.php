<?php 

use Brain\Monkey\Functions;
use \App\Core\SettingsLinks;

class SettingsLinksTest extends \PluginTestCase {

    public function test_settingsLink()
    {
        $class = new SettingsLinks;

        $links = [];
        $settingsLink = '<a href="admin.php?page=user_table_plugin">Settings</a>';
        array_push($links, $settingsLink);
        
        $array = $class->settingsLink([]);

        $this->assertSame($links, $array);
    }

}