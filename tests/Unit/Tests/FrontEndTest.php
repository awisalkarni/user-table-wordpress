<?php 

use Brain\Monkey\Functions;
use \App\Pages\FrontEnd;

class FronEndTest extends \PluginTestCase {

    public function test_register()
    {
        $object = new FrontEnd;

        Functions\expect('add_action')
        ->once()
        ->with('init', [$object, 'init'])
        ->andReturn(true);

        Functions\expect('add_action')
        ->once()
        ->with('query_vars', [$object, 'queryVars'])
        ->andReturn(true);

        Functions\expect('add_action')
        ->once()
        ->with('parse_request', [$object, 'parseRequest'])
        ->andReturn(true);

        $object->register();
    }

    public function test_customEndpoint()
    {
        Functions\expect('get_option')
        ->once()
        ->with('custom_endpoint')
        ->andReturn('awis-custom-endpoint');

        Functions\expect('esc_attr')
        ->once()
        ->with('awis-custom-endpoint')
        ->andReturn('awis-custom-endpoint');

        $object = new FrontEnd;
        $return = $object->customEndpoint();
    }

    public function test_init()
    {
        Functions\when('get_option')->justReturn('awis-custom-endpoint');
        Functions\when('esc_attr')->justReturn('');

        Functions\expect('add_rewrite_rule')
        ->once()
        ->with(Mockery::type('string'), Mockery::type('string'), Mockery::type('string'))
        ->andReturn(true);

        Functions\expect('flush_rewrite_rules')
        ->once()
        ->with(true);

        $object = new FrontEnd;
        $object->init();
    }

    public function test_makeHttpRequest()
    {
        $object = new FrontEnd;
        $reflection = new \ReflectionClass('\App\Pages\FrontEnd');

        $method = $reflection->getMethod('makeHttpRequest');
        $method->setAccessible(true);

        $mockJson = '[{"id":1,"name":"Leanne Graham","username":"Bret","email":"Sincere@april.biz","address":{"street":"Kulas Light","suite":"Apt. 556","city":"Gwenborough","zipcode":"92998-3874","geo":{"lat":"-37.3159","lng":"81.1496"}},"phone":"1-770-736-8031 x56442","website":"hildegard.org","company":{"name":"Romaguera-Crona","catchPhrase":"Multi-layered client-server neural-net","bs":"harness real-time e-markets"}}]';

        Functions\when('wp_remote_get')->justReturn($mockJson);

        Functions\when('get_transient')->justReturn($mockJson);

        $response = $method->invokeArgs($object, []);

        $this->assertSame($response, $mockJson);
    }

}