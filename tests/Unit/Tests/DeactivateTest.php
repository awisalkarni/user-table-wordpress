<?php 

use Brain\Monkey\Functions;
use \App\Core\Deactivate;

class DeactivateTest extends \PluginTestCase {

    function testDeactivate() {

        Functions\expect('flush_rewrite_rules')
        ->once()
        ->with(true)
        ->andReturn(null);

        $class = new Deactivate;
        $class->deactivatePlugin();
    }

}