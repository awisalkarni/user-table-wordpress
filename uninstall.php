<?php declare(strict_types=1);
/**
*
* @package User Table
*/

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

//clear database
delete_option('custom_endpoint');
delete_transient('user_table_users_request');
