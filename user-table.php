<?php declare(strict_types=1);
/**
* Plugin Name
*
* @package           User Table
* @author            Awis Alkarni Ghazali
* @copyright         2020 Awis Alkarni
* @license           GPL-2.0-or-later
*
* @wordpress-plugin
* Plugin Name:       User Table
* Plugin URI:        https://awislabs.com/plugin-name
* Description:       Inpsyde test plugin
* Version:           1.0.0
* Requires at least: 5.2
* Requires PHP:      7.2
* Author:            Awis Alkarni Ghazali
* Author URI:        https://awislabs.com
* Text Domain:       user-table
* License:           GPL v2 or later
* License URI:       http://www.gnu.org/licenses/gpl-2.0.txt

User Table is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

User Table is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with User Table. If not, see http://www.gnu.org/licenses/gpl-2.0.txt.
*/

defined('ABSPATH') or die();

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

use App\Core\Activate;
use App\Core\Deactivate;

register_activation_hook(__FILE__, function () {
    Activate::activatePlugin();
});

register_deactivation_hook(__FILE__, function () {
    Deactivate::deactivatePlugin();
});

if (class_exists('App\\Init')) {
    App\Init::register();
}
