<?php declare(strict_types=1); ?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" 
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
    crossorigin="anonymous">

    <style>
        body {
          padding-top: 5rem;
      }
      

      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      table {
        table-layout:fixed;
        width:100%;
    }

    td {
        word-wrap:break-word;
        word-break:break-all;
    }

    ul {
        padding-left: 10px;
    }
    </style>

    <title>Hello, world!</title>
  </head>
  <body>
   

<main role="main" class="container">

    <div class="row">
        <div class="col-6">
            <h1>Users</h1>
            <table id="user-table" class="table table-bordered">
                <thead></thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="col-6">
            <h1>Details</h1>
            <div id="detail" class="card" style="width: 18rem;">
                
                <div class="card-body">
                    <div id="loading-spinner" class="spinner-border" role="status">
                      <span class="sr-only">Loading...</span>
                  </div>
                    <h5 class="card-title"></h5>
                    <p class="card-text"></p>
                </div>
            </div>
            
        </div>
    </div>
  

</main><!-- /.container -->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
    crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
    crossorigin="anonymous"></script>

    <script type="text/javascript">
        var table = $('#user-table');
        var requiredFields = ["id", "name", "username", "email"];
        var json = <?php echo json_encode(json_decode($json)) ?>;

        var spinner = $('#loading-spinner');

        spinner.hide();
        
        $.each(json, function(position, object){
            $('#user-table tbody').append('<tr></tr>')

            if (position == 0) {
                showUserDetails(object.id)
            }

            $.each(object, function(key, value){
                if (position == 0) {
                    if ($.inArray(key, requiredFields) != -1) {
                        $('#user-table thead').append('<th>'+key+'</th>')
                    }



                }

                if ($.inArray(key, requiredFields) != -1) {
                    if (value instanceof Object) {
                        $.each(value, function(ok, ov) {
                            $('#user-table tbody').append(ok+': '+ov+'<br>')
                        })
                    } else {
                        $('#user-table tbody')
                        .append('<td><a onclick="showUserDetails('+object.id+')" href="#" class="btn btn-link user-detail">'+value+'</a></td>')
                    }
                }
            });

            
        });

        function showUserDetails(id)
        {
            spinner.show();

            $('#detail h5').empty();
            $('#detail p').empty();
            
            $.get('https://jsonplaceholder.typicode.com/users/'+id, function(data){
                renderUserDetails(data)
                spinner.hide();
            });
        }

        function renderUserDetails(data)
        {
            $('#detail h5').html(data.name)
            
            $.each(data, function(key, value){
                if (value instanceof Object) {
                    $('#detail p').append('<strong>'+titleCase(key)+': </strong>')
                    
                    $.each(value, function(ok, ov) {
                        if (ov instanceof Object) {
                            $('#detail p').append('<li><strong>'+titleCase(ok)+': </strong>')

                            
                            $.each(ov, function(ook, oov) {
                                $('#detail p').append('<li>' + titleCase(ook) + ': ' + oov + '</li>')
                            })
                            
                        } else {
                           $('#detail p').append('<li>' + titleCase(ok) + ': ' + ov + '</li>')
                        }
                    })
                    

                } else {
                    $('#detail p').append('<strong>' + titleCase(key) + '</strong>: ' + value + '<br>')
                }
            })
            
        }

        function titleCase(string) {
            var sentence = string.toLowerCase().split(" ");
            for(var i = 0; i< sentence.length; i++){
                sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
            }
            return sentence;
        }

        
    </script>
  </body>
</html>