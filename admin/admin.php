<?php declare(strict_types=1); ?>

<div class="wrap">
    <h1>User Table Plugin</h1>
    <?php settings_errors(); ?>

    <form method="POST" action="options.php">
        <?php
        settings_fields('usertable_options_group');
        do_settings_sections('user_table_plugin');
        ?>

        <?php if (!is_null(get_option('custom_endpoint'))) : ?>
            <?php $currentUrl = get_home_url() . '/' . get_option('custom_endpoint') ?>
            Current custom endpoint is 
            <a target="blank" href="<?php echo esc_url($currentUrl) ?>">
                <?php echo esc_url($currentUrl) ?>
            </a>
        <?php endif; ?>

        <?php
        submit_button();
        ?>
    </form>
</div>

