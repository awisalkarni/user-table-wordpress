<?php declare(strict_types=1);

/**
*
* @package User Table
*/

namespace App\Core;

class BaseApp
{
    protected $pluginPath;
    protected $pluginUrl;
    protected $plugin;

    public function __construct()
    {
        $this->pluginPath = plugin_dir_path(dirname(__FILE__, 2));
        $this->pluginUrl = plugin_dir_url(dirname(__FILE__, 2));
        $this->plugin = plugin_basename(dirname(__FILE__, 3)) . '/user-table.php';
    }
}
