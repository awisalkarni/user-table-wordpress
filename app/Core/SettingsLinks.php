<?php declare(strict_types=1);
/**
*
* @package User Table
*/

namespace App\Core;

use \App\Core\BaseApp;

class SettingsLinks extends BaseApp
{
    public function register()
    {
        add_filter('plugin_action_links_' . $this->plugin, [$this, 'settingsLink']);
    }

    public function settingsLink(array $links) : array
    {
        $settingsLink = '<a href="admin.php?page=user_table_plugin">Settings</a>';
        array_push($links, $settingsLink);
        return $links;
    }
}
