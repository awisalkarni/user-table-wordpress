<?php declare(strict_types=1);
/**
*
* @package User Table
*/

namespace App\Core;

class Activate
{
    public static function activatePlugin()
    {
        add_option('custom_endpoint', 'awis-custom-endpoint');
        flush_rewrite_rules(true);
    }
}
