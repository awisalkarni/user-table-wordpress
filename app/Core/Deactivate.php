<?php declare(strict_types=1);
/**
*
* @package User Table
*/

namespace App\Core;

class Deactivate
{
    public static function deactivatePlugin()
    {
        flush_rewrite_rules(true);
    }
}
