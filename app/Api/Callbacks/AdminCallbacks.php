<?php declare(strict_types=1);
/**
*
* @package User Table
*/

namespace App\Api\Callbacks;

use App\Core\BaseApp;

class AdminCallbacks extends BaseApp
{
    public function adminDashboard() : int
    {
        return require_once $this->pluginPath . 'admin/admin.php';
    }

    public function usertableOptionsGroup(string $input) : string
    {
        return $input;
    }

    public function usertableSectionsGroup(array $input)
    {
        echo 'Enter your custom. 
        With “custom endpoint” we mean an arbitrary URL not recognized by WP as a standard URL, 
        like a permalink or so.';
    }

    public function usertableUserUrlField(array $input)
    {
        $value = esc_attr(get_option('custom_endpoint'));
        echo wp_kses('<input type="text" 
            class="" name="custom_endpoint" 
            value="'.$value.'" 
            placeholder="Enter your custom endpoint here">', [
            'input' => [
                'type' => [],
                'name' => [],
                'value' => [],
                'placeholder' => [],
            ],
        ]);
    }
}
