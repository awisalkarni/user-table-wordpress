<?php declare(strict_types=1);
/**
* @package User Table
*/

namespace App\Api;

class SettingsApi
{
    protected $adminPages = [];
    protected $settings = [];
    protected $sections = [];
    protected $fields = [];

    public function register()
    {
        if (!empty($this->adminPages)) {
            add_action('admin_menu', [$this, 'addAdminMenu']);
        }

        if (!empty($this->settings)) {
            add_action('admin_init', [$this, 'registerCustomFields']);
        }
    }

    public function addPages(array $pages) : SettingsApi
    {
        $this->adminPages = $pages;
        return $this;
    }

    public function addAdminMenu()
    {
        foreach ($this->adminPages as $page) {
            add_menu_page(
                $page['page_title'],
                $page['menu_title'],
                $page['capability'],
                $page['menu_slug'],
                $page['callback'],
                $page['icon_url'],
                $page['position']
            );
        }
    }

    public function addSettings(array $settings) : SettingsApi
    {
        $this->settings = $settings;
        return $this;
    }

    public function addSections(array $sections) : SettingsApi
    {
        $this->sections = $sections;
        return $this;
    }

    public function addFields(array $fields) : SettingsApi
    {
        $this->fields = $fields;
        return $this;
    }

    public function registerCustomFields()
    {
        // register setting
        foreach ($this->settings as $setting) {
            register_setting(
                $setting["option_group"],
                $setting["option_name"],
                (isset($setting["callback"]) ? $setting["callback"] : '' )
            );
        }

        // add settings section
        foreach ($this->sections as $section) {
            add_settings_section(
                $section["id"],
                $section["title"],
                (isset($section["callback"]) ? $section["callback"] : '' ),
                $section["page"]
            );
        }

        // add settings field
        foreach ($this->fields as $field) {
            add_settings_field(
                $field["id"],
                $field["title"],
                (isset($field["callback"]) ? $field["callback"] : '' ),
                $field["page"],
                $field["section"],
                ( isset($field["args"]) ? $field["args"] : '' )
            );
        }
    }
}
