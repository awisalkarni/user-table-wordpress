<?php declare(strict_types=1);
namespace App;

/**
*
* @package User Table
*/

final class Init
{
    public static function services() : array
    {
        return [
            Pages\Admin::class,
            Core\Enqueue::class,
            Core\SettingsLinks::class,
            Pages\FrontEnd::class,
        ];
    }

    /**
    * Loop through classes and init them
    * @return array List of classes
    */
    public static function register()
    {
        foreach (self::services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }

    /**
    * Init class
    * @param class
    * @return class instance List of classes
    */
    private static function instantiate(string $class) : \App\Core\BaseApp
    {
        $service = new $class();
        return $service;
    }
}
