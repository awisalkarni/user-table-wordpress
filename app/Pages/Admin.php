<?php declare(strict_types=1);
/**
*
* @package User Table
*/
namespace App\Pages;

use \App\Core\BaseApp;
use \App\Api\SettingsApi;
use \App\Api\Callbacks\AdminCallbacks;

class Admin extends BaseApp
{

    protected $settings;
    protected $pages;
    protected $callbacks;

    public function register()
    {
        $this->settings = new SettingsApi;
        $this->callbacks = new AdminCallbacks;
        $this->initPages();

        $this->initSettings();
        $this->initSections();
        $this->initFields();
        $this->settings->addPages($this->pages)->register();
    }

    public function adminIndex()
    {
        require_once $this->pluginPath . 'admin/admin.php';
    }

    public function initPages()
    {
        $this->pages = [
            [
                'page_title' => 'User Table',
                'menu_title' => 'User Table',
                'capability' => 'manage_options',
                'menu_slug' => 'user_table_plugin',
                'callback' => [$this->callbacks, 'adminDashboard'],
                'icon_url' => 'dashicons-admin-site-alt3',
                'position' => 100,
            ],
        ];
    }

    public function initSettings()
    {
        $args = [
            [
                'option_group' => 'usertable_options_group',
                'option_name' => 'custom_endpoint',
                'callback' => [$this->callbacks, 'usertableOptionsGroup'],
            ],
        ];

        $this->settings->addSettings($args);
    }

    public function initSections()
    {
        $args = [
            [
                'id' => 'usertable_admin_index',
                'title' => 'User URL',
                'callback' => [$this->callbacks, 'usertableSectionsGroup'],
                'page' => 'user_table_plugin',
            ],
        ];

        $this->settings->addSections($args);
    }

    public function initFields()
    {
        $args = [
            [
                'id' => 'custom_endpoint',
                'title' => 'Custom endpoint',
                'callback' => [$this->callbacks, 'usertableUserUrlField'],
                'page' => 'user_table_plugin',
                'section' => 'usertable_admin_index',
                'args' => [
                    'label_for' => 'custom_endpoint',
                    'class' => 'input-control',
                ],
            ],
        ];

        $this->settings->addFields($args);
    }
}
