<?php declare(strict_types=1);
namespace App\Pages;

/**
*
* @package User Table
*/

use \App\Core\BaseApp;

class FrontEnd extends BaseApp
{

    public function customEndpoint() : string
    {
        return esc_attr(get_option('custom_endpoint'));
    }

    public function register()
    {
        add_action('init', [$this, 'init']);
        add_action('query_vars', [$this, 'queryVars']);
        add_action('parse_request', [$this, 'parseRequest']);
    }

    public function init()
    {
        add_rewrite_rule(
            '^'.$this->customEndpoint().'$',
            'index.php?'.$this->customEndpoint().'=true',
            'top'
        );
        flush_rewrite_rules(true);
    }

    public function queryVars(array $queryVars) : array
    {
        $queryVars[] = $this->customEndpoint();
        return $queryVars;
    }

    public function parseRequest(\WP &$wp)
    {
        if (array_key_exists($this->customEndpoint(), $wp->query_vars)) {
            $json = $this->makeHttpRequest();
            include($this->pluginPath . '/public/index.php');
            exit();
        }
    }

    private function makeHttpRequest() : string
    {
        $transientName = 'user_table_users_request';
        $transientValue = get_transient($transientName);

        if (false !== $transientValue) {
            $json = $transientValue;
            return $json;
        }

        $api = "https://jsonplaceholder.typicode.com/";
        $endpoint = "users/";

        try {
            $response = wp_remote_get($api . $endpoint);
            if (!is_wp_error($response)) {
                $json = $response["body"];
                $expirationTime = 60; //60 seconds expiration to test
                set_transient($transientName, $json, $expirationTime);
                return $json;
            }
            die(esc_html($response->get_error_message()));
        } catch (Exception $exception) {
            $json = null;
        }
    }
}
