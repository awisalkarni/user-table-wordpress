## About Plugin

This is a wordpress plugin with full [composer](http://getcomposer.org/) support. 

What it does is allow users to set custom endpoint via admin menu and expose a custom html to visitor when the navigate to it.

Admin Features:

* Can view custom endpoint.
* Can edit custom endpoint.


Frontend Features: 

* When a visitor navigates to that endpoint, the plugin will send an HTTP request to a REST API endpoint. The API is available at [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/) and the endpoint to call is /users.
* The plugin will parse the JSON response and will use it to build and display an HTML table.
* Each row in the HTML table will show the details for a user. 
* These details fetching requests must be asynchronous (AJAX) and the user details will be shown without reloading the page.
* HTTP request is done via controller (wp_remote_get) and cached using wordpress built in transient API.

---
## Requirements

* [Wordpress](https://wordpress.org/) >= v4.9.6
* [Composer](https://getcomposer.org/download/) >= v1.6.5
* [PHP](http://php.net/manual/en/install.php) >= v7.0

---

## Structure

```
├── README.md
├── admin
│   └── admin.php
├── app
│   ├── Api
│   │   ├── Callbacks
│   │   │   └── AdminCallbacks.php
│   │   └── SettingsApi.php
│   ├── Core
│   │   ├── Activate.php
│   │   ├── BaseApp.php
│   │   ├── Deactivate.php
│   │   ├── Enqueue.php
│   │   └── SettingsLinks.php
│   ├── Init.php
│   └── Pages
│       ├── Admin.php
│       └── FrontEnd.php
├── assets
│   ├── scripts.js
│   └── style.css
├── composer.json
├── composer.lock
├── index.php
├── phpcs.xml.dist
├── phpunit.xml.dist
├── public
│   └── index.php
├── tests
│   └── Unit
│       ├── Inc
│       │   └── PluginTestCase.php
│       ├── Tests
│       │   ├── ActivateTest.php
│       │   ├── AdminCallbacksTest.php
│       │   ├── AdminTest.php
│       │   ├── DeactivateTest.php
│       │   ├── FrontEndTest.php
│       │   ├── InitTest.php
│       │   ├── SettingsApiTest.php
│       │   └── SettingsLinksTest.php
│       └── bootstrap.php
├── uninstall.php
└── user-table.php

```

---
## Approches

### PSR4 Autoloading using composer
```
"autoload": {
	"psr-4": {"App\\": "./app"}
}
```

### Using `add_rewrite_rule` to enable custom endpoint

1. I'm using `add_rewrite_rule` and query vars to set the custom endpoint. Example: `/index.php?awis-custom-endpoint=true`
2. `flush_rewrite_rules` was called during this. I know it should be only used carefully because it's an expensive function such as at activate and deactivate hook but to ensure that it works at 100% time for this test, I decided to call it here. 
3. I added parse_request action that compares the query vars to the custom endpoint and if they match, it'll do a `wp_remote_get` to get json and include a custom template.

### JSON cache using WP Transient API

```
$transientName = 'user_table_users_request';
$transientValue = get_transient($transientName);

if (false !== $transientValue) {
	$json = $transientValue;
	return $json;
}

$api = "https://jsonplaceholder.typicode.com/";
$endpoint = "users/";

$response = wp_remote_get($api . $endpoint);

try {
	$json = $response["body"];
	$expirationTime = 5; //5 seconds expiration to test
	set_transient($transientName, $json, $expirationTime);
	return $json;
} catch (Exception $exception) {
	$json = null;
}
```

### Inpsyde PHP Coding Standards

100% follows [Inpsyde PHP Coding Standards](https://github.com/inpsyde/php-coding-standards).

To run phpcs `vendor/bin/phpcs`


### Supports unit testing using Brain Monkey and Mockery

To run phpunit, `vendor/bin/phpunit`

### 
---

## Installation

* Add custom respository to your composer.json

### http
```json
"repositories":[
	{
		"type": "vcs",
		"url":  "https://awisghazali@bitbucket.org/awisghazali/user-table.git"
	}
]
```
### ssh
```json
"repositories":[
	{
		"type": "vcs",
		"url":  "git@bitbucket.org:awisghazali/user-table.git"
	}
]
```

* run `composer require awisghazali/user-table dev-master` 

#### or

* add `awisghazali/user-table` to `require` section on your composer.json
* run `composer install` 

---

## Usage instruction

1. Activate plugin in wordpress plugin page.
2. Visit User Table menu in wordpress admin dashboard.
3. Change the custom endpoint to your preferred endpoint.
4. Visit the endpoint with the link provided.

## License

User Table is open-sourced software licensed under the [GPL v2 or later license](http://www.gnu.org/licenses/gpl-2.0.txt).
